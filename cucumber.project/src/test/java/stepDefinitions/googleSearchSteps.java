package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class googleSearchSteps {
	
	WebDriver driver;
	
	@Given("open www.google.com")
	public void open_www_google_com() {
		
		System.setProperty("webdriver.chrome.driver", "D:\\nss\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http:/www.google.com");
	    // Write code here that turns the phrase above into concrete actions
//	    throw new cucumber.api.PendingException();
	}

	@When("type BDD in search box and hit enter")
	public void type_BDD_in_search_box_and_hit_enter() {
	    // Write code here that turns the phrase above into concrete actions
//	    throw new cucumber.api.PendingException();
	}

	@Then("should display BDD search results")
	public void should_display_BDD_search_results() {
	    // Write code here that turns the phrase above into concrete actions
//	    throw new cucumber.api.PendingException();
	}

}
